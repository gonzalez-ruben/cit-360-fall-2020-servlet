package SimpleServlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "Servlet", urlPatterns = {"/Servlet"})
public class Servlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        try {
            if (!firstName.matches("[\\w*\\s*]*") | !lastName.matches("[\\w*\\s*]*")) {
                out.println("<html><head><title>Error!</title></head><body><h1>Invalid input.</h1><p><a href=\"index.html\">Please click here to try again.</a></p> </body></html>");
            } else {
                out.println("<html><head><title> Welcome " + firstName + " " + lastName + " !</title><link rel=\"stylesheet\" href=\"style2.css\"></head><body>");
                out.println("<h1>Hello!</h1> <hr><h2>My Name Is</h2>");
                out.println("<h3>" + firstName + " " + lastName + "</h3>");
                out.println("<p>Pleasure to meet you</p></body></html>");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("Please use the website to complete this request.");
    }
}
